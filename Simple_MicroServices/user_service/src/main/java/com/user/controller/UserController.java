package com.user.controller;

import com.user.entity.User;
import com.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService service;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/id/{id}")
    public User getUser(@PathVariable Long id){
        List contacts= this.restTemplate.getForObject("http://contact-service/contact/user/"+id,List.class);
        User user=this.service.getUser(id);
        user.setContacts(contacts);
        return user;
//        http://localhost:9002/contact/user/14

    }



//    @GetMapping("all")
//    public List<User> getall(){
//        return service.getAll();
//    }
}
