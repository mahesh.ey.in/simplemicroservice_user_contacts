package com.user.service;

import com.user.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService{


    //dummy user data
    List<User> list=List.of(
            new User(12L,"mahesh","82782"),
            new User(13L,"rohan","12782"),
            new User(14L,"jacky","012782"));


    @Override
    public User getUser(Long id) {
        return this.list.stream().filter(user->user.getUserId().equals(id))
                .findAny().orElse(null);
    }

    @Override
    public List<User> getAll() {
        return this.list;
    }

}
