package com.contact.service;

import com.contact.entity.Contact;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContactServiceImpl implements ContactService {
    //dummy contact data
    List<Contact> list = List.of(
            new Contact(1L, "jacky123@mail.com", "jacky", 14L),
            new Contact(2L, "jacky12331@mail.com", "jacky", 14L),

            new Contact(3L, "mahi12@mail.com", "mahesh", 12L),
            new Contact(4L, "rohan6711@mail.com", "rohan", 13L)

    );

    @Override
    public List<Contact> getContactOfUser(Long userId) {
        return this.list.stream().filter(contact->contact.getUserId()
        .equals(userId)).collect(Collectors.toList());
    }
}
